import java.util.Arrays;
import java.util.Random;

public class App 
{
    public static void main(String args[]) {
        
        int size = 100;
        Random liczba = new Random();
        int[] tab = new int[size];
        
        for(int i = 0; i < size; i++)
        {
            tab[i] = liczba.nextInt(10);
        }
        
        Arrays.sort(tab);
        
        int temp;
        for(int i = 0; i < size / 2; i++)
        {
            temp = tab[i];
            tab[i] = tab[size - i - 1];
            tab[size - i - 1] = temp;
        }
        
        for(int i = 0; i < size; i++)
        {
            System.out.println(tab[i]);
        }
        
        System.out.println("maks = " + tab[0]);
        System.out.println("min = " + tab[size - 1]);
    
    }
}